﻿namespace JTTT
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.urlTextbox = new System.Windows.Forms.TextBox();
            this.tagTextbox = new System.Windows.Forms.TextBox();
            this.emailTextbox = new System.Windows.Forms.TextBox();
            this.wyslijButton = new System.Windows.Forms.Button();
            this.zadaniaBox = new System.Windows.Forms.ListBox();
            this.wykonajButton = new System.Windows.Forms.Button();
            this.czyscButton = new System.Windows.Forms.Button();
            this.serializujButton = new System.Windows.Forms.Button();
            this.eserializujButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dodajZadanieBox = new System.Windows.Forms.TextBox();
            this.CzyscOstatnie = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageStrona = new System.Windows.Forms.TabPage();
            this.tabPagePogoda = new System.Windows.Forms.TabPage();
            this.TempNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.MiastoTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mailOkno = new System.Windows.Forms.TabControl();
            this.tabPageMail = new System.Windows.Forms.TabPage();
            this.tabPageOkno = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPageStrona.SuspendLayout();
            this.tabPagePogoda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TempNumericUpDown)).BeginInit();
            this.mailOkno.SuspendLayout();
            this.tabPageMail.SuspendLayout();
            this.tabPageOkno.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(270, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jeżeli";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(39, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "to:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(348, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Na tej stronie znajdziesz obrazek, którego podpis zawiera tekst";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "URL:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tekst:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(43, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "to wykonaj to:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(3, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(235, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Wyślij na podany adres email wiadomość ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "e-mail: ";
            // 
            // urlTextbox
            // 
            this.urlTextbox.Location = new System.Drawing.Point(109, 43);
            this.urlTextbox.Name = "urlTextbox";
            this.urlTextbox.Size = new System.Drawing.Size(100, 20);
            this.urlTextbox.TabIndex = 8;
            // 
            // tagTextbox
            // 
            this.tagTextbox.Location = new System.Drawing.Point(109, 70);
            this.tagTextbox.Name = "tagTextbox";
            this.tagTextbox.Size = new System.Drawing.Size(100, 20);
            this.tagTextbox.TabIndex = 9;
           
            // 
            // emailTextbox
            // 
            this.emailTextbox.Location = new System.Drawing.Point(49, 40);
            this.emailTextbox.Name = "emailTextbox";
            this.emailTextbox.Size = new System.Drawing.Size(100, 20);
            this.emailTextbox.TabIndex = 10;
            // 
            // wyslijButton
            // 
            this.wyslijButton.Location = new System.Drawing.Point(335, 346);
            this.wyslijButton.Name = "wyslijButton";
            this.wyslijButton.Size = new System.Drawing.Size(117, 23);
            this.wyslijButton.TabIndex = 11;
            this.wyslijButton.Text = "Dodaj do listy zadań";
            this.wyslijButton.UseVisualStyleBackColor = true;
            this.wyslijButton.Click += new System.EventHandler(this.wyslijButton_Click);
            // 
            // zadaniaBox
            // 
            this.zadaniaBox.FormattingEnabled = true;
            this.zadaniaBox.Location = new System.Drawing.Point(456, 50);
            this.zadaniaBox.Margin = new System.Windows.Forms.Padding(1);
            this.zadaniaBox.Name = "zadaniaBox";
            this.zadaniaBox.Size = new System.Drawing.Size(436, 186);
            this.zadaniaBox.TabIndex = 12;
            // 
            // wykonajButton
            // 
            this.wykonajButton.Location = new System.Drawing.Point(474, 247);
            this.wykonajButton.Margin = new System.Windows.Forms.Padding(1);
            this.wykonajButton.Name = "wykonajButton";
            this.wykonajButton.Size = new System.Drawing.Size(81, 49);
            this.wykonajButton.TabIndex = 13;
            this.wykonajButton.Text = "Wykonaj";
            this.wykonajButton.UseVisualStyleBackColor = true;
            this.wykonajButton.Click += new System.EventHandler(this.wykonajButton_Click);
            // 
            // czyscButton
            // 
            this.czyscButton.Location = new System.Drawing.Point(579, 247);
            this.czyscButton.Margin = new System.Windows.Forms.Padding(1);
            this.czyscButton.Name = "czyscButton";
            this.czyscButton.Size = new System.Drawing.Size(84, 49);
            this.czyscButton.TabIndex = 14;
            this.czyscButton.Text = "Czyść";
            this.czyscButton.UseVisualStyleBackColor = true;
            this.czyscButton.Click += new System.EventHandler(this.czyscButton_Click);
            // 
            // serializujButton
            // 
            this.serializujButton.Location = new System.Drawing.Point(681, 243);
            this.serializujButton.Margin = new System.Windows.Forms.Padding(1);
            this.serializujButton.Name = "serializujButton";
            this.serializujButton.Size = new System.Drawing.Size(86, 23);
            this.serializujButton.TabIndex = 15;
            this.serializujButton.Text = "Serializuj";
            this.serializujButton.UseVisualStyleBackColor = true;
            this.serializujButton.Click += new System.EventHandler(this.serializujButton_Click);
            // 
            // eserializujButton
            // 
            this.eserializujButton.Location = new System.Drawing.Point(681, 273);
            this.eserializujButton.Margin = new System.Windows.Forms.Padding(1);
            this.eserializujButton.Name = "eserializujButton";
            this.eserializujButton.Size = new System.Drawing.Size(86, 23);
            this.eserializujButton.TabIndex = 16;
            this.eserializujButton.Text = "deserializuj";
            this.eserializujButton.UseVisualStyleBackColor = true;
            this.eserializujButton.Click += new System.EventHandler(this.eserializujButton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(475, 27);
            this.label9.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Lista zadań: ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(41, 346);
            this.label10.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Podaj nazwę zadania";
            // 
            // dodajZadanieBox
            // 
            this.dodajZadanieBox.Location = new System.Drawing.Point(166, 339);
            this.dodajZadanieBox.Margin = new System.Windows.Forms.Padding(1);
            this.dodajZadanieBox.Name = "dodajZadanieBox";
            this.dodajZadanieBox.Size = new System.Drawing.Size(100, 20);
            this.dodajZadanieBox.TabIndex = 19;
            // 
            // CzyscOstatnie
            // 
            this.CzyscOstatnie.Location = new System.Drawing.Point(579, 298);
            this.CzyscOstatnie.Name = "CzyscOstatnie";
            this.CzyscOstatnie.Size = new System.Drawing.Size(84, 49);
            this.CzyscOstatnie.TabIndex = 20;
            this.CzyscOstatnie.Text = "Czyść ostatnie";
            this.CzyscOstatnie.UseVisualStyleBackColor = true;
            this.CzyscOstatnie.Click += new System.EventHandler(this.CzyscOstatnie_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageStrona);
            this.tabControl1.Controls.Add(this.tabPagePogoda);
            this.tabControl1.Location = new System.Drawing.Point(44, 78);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(408, 124);
            this.tabControl1.TabIndex = 22;
            // 
            // tabPageStrona
            // 
            this.tabPageStrona.Controls.Add(this.label3);
            this.tabPageStrona.Controls.Add(this.label4);
            this.tabPageStrona.Controls.Add(this.urlTextbox);
            this.tabPageStrona.Controls.Add(this.label5);
            this.tabPageStrona.Controls.Add(this.tagTextbox);
            this.tabPageStrona.Location = new System.Drawing.Point(4, 22);
            this.tabPageStrona.Name = "tabPageStrona";
            this.tabPageStrona.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStrona.Size = new System.Drawing.Size(400, 98);
            this.tabPageStrona.TabIndex = 0;
            this.tabPageStrona.Text = "Sprawdz strone";
            this.tabPageStrona.UseVisualStyleBackColor = true;
           // this.tabPageStrona.Click += new System.EventHandler(this.tabPageStrona_Click);
            // 
            // tabPagePogoda
            // 
            this.tabPagePogoda.Controls.Add(this.TempNumericUpDown);
            this.tabPagePogoda.Controls.Add(this.MiastoTextBox);
            this.tabPagePogoda.Controls.Add(this.label13);
            this.tabPagePogoda.Controls.Add(this.label12);
            this.tabPagePogoda.Controls.Add(this.label11);
            this.tabPagePogoda.Location = new System.Drawing.Point(4, 22);
            this.tabPagePogoda.Name = "tabPagePogoda";
            this.tabPagePogoda.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePogoda.Size = new System.Drawing.Size(400, 98);
            this.tabPagePogoda.TabIndex = 1;
            this.tabPagePogoda.Text = "Sprawdz pogode";
            this.tabPagePogoda.UseVisualStyleBackColor = true;      
            // 
            // TempNumericUpDown
            // 
            this.TempNumericUpDown.Location = new System.Drawing.Point(83, 71);
            this.TempNumericUpDown.Name = "TempNumericUpDown";
            this.TempNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.TempNumericUpDown.TabIndex = 6;
            this.TempNumericUpDown.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // MiastoTextBox
            // 
            this.MiastoTextBox.Location = new System.Drawing.Point(83, 43);
            this.MiastoTextBox.Name = "MiastoTextBox";
            this.MiastoTextBox.Size = new System.Drawing.Size(120, 20);
            this.MiastoTextBox.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 73);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Temperatura:";
            //this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 43);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Miasto:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(3, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(244, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "W mieście panuje większa temperatura niż:";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // mailOkno
            // 
            this.mailOkno.Controls.Add(this.tabPageMail);
            this.mailOkno.Controls.Add(this.tabPageOkno);
            this.mailOkno.Location = new System.Drawing.Point(48, 233);
            this.mailOkno.Name = "mailOkno";
            this.mailOkno.SelectedIndex = 0;
            this.mailOkno.Size = new System.Drawing.Size(270, 102);
            this.mailOkno.TabIndex = 23;
            // 
            // tabPageMail
            // 
            this.tabPageMail.Controls.Add(this.label8);
            this.tabPageMail.Controls.Add(this.emailTextbox);
            this.tabPageMail.Controls.Add(this.label7);
            this.tabPageMail.Location = new System.Drawing.Point(4, 22);
            this.tabPageMail.Name = "tabPageMail";
            this.tabPageMail.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMail.Size = new System.Drawing.Size(262, 76);
            this.tabPageMail.TabIndex = 0;
            this.tabPageMail.Text = "Mail";
            this.tabPageMail.UseVisualStyleBackColor = true;
            // 
            // tabPageOkno
            // 
            this.tabPageOkno.Controls.Add(this.label14);
            this.tabPageOkno.Location = new System.Drawing.Point(4, 22);
            this.tabPageOkno.Name = "tabPageOkno";
            this.tabPageOkno.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOkno.Size = new System.Drawing.Size(262, 76);
            this.tabPageOkno.TabIndex = 1;
            this.tabPageOkno.Text = "Okno";
            this.tabPageOkno.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(39, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(152, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Pokaż informacje w oknie";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 381);
            this.Controls.Add(this.mailOkno);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.CzyscOstatnie);
            this.Controls.Add(this.dodajZadanieBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.eserializujButton);
            this.Controls.Add(this.serializujButton);
            this.Controls.Add(this.czyscButton);
            this.Controls.Add(this.wykonajButton);
            this.Controls.Add(this.zadaniaBox);
            this.Controls.Add(this.wyslijButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "JTTT";
            this.tabControl1.ResumeLayout(false);
            this.tabPageStrona.ResumeLayout(false);
            this.tabPageStrona.PerformLayout();
            this.tabPagePogoda.ResumeLayout(false);
            this.tabPagePogoda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TempNumericUpDown)).EndInit();
            this.mailOkno.ResumeLayout(false);
            this.tabPageMail.ResumeLayout(false);
            this.tabPageMail.PerformLayout();
            this.tabPageOkno.ResumeLayout(false);
            this.tabPageOkno.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox urlTextbox;
        private System.Windows.Forms.TextBox tagTextbox;
        private System.Windows.Forms.TextBox emailTextbox;
        private System.Windows.Forms.Button wyslijButton;
        private System.Windows.Forms.ListBox zadaniaBox;
        private System.Windows.Forms.Button wykonajButton;
        private System.Windows.Forms.Button czyscButton;
        private System.Windows.Forms.Button serializujButton;
        private System.Windows.Forms.Button eserializujButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox dodajZadanieBox;
        private System.Windows.Forms.Button CzyscOstatnie;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageStrona;
        private System.Windows.Forms.TabPage tabPagePogoda;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox MiastoTextBox;
        private System.Windows.Forms.NumericUpDown TempNumericUpDown;
        private System.Windows.Forms.TabControl mailOkno;
        private System.Windows.Forms.TabPage tabPageMail;
        private System.Windows.Forms.TabPage tabPageOkno;
        private System.Windows.Forms.Label label14;
    }
}

