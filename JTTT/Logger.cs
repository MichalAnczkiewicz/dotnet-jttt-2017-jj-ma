﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    class Logger
    {
        public string adresStrony;
        public string tag;
        public string email;

        public Logger() { }
        public Logger(string adresStrony, string tag, string email)
        {
            this.adresStrony = adresStrony;
            this.tag = tag;
            this.email = email;
        }

        public void Logowanie(string PrzebiegOperacji)
        {
            string ZawartoscPliku;
            if(File.Exists("JTTT.log"))
            {
                ZawartoscPliku = File.ReadAllText("JTTT.log");
            }
            else
            {
                ZawartoscPliku = "";
            }

            if (adresStrony == null || tag == null || email == null)
            {
                ZawartoscPliku += ("Czas: " + DateTime.Now + " Operacja wyswietlania stanu pogody:" + " " + PrzebiegOperacji + "\r\n");
            }
            else
            {
                ZawartoscPliku += ("Czas: " + DateTime.Now + " Tag: " + this.tag + " Adresat: " + this.email + " " + PrzebiegOperacji + "\r\n");
            }

            File.WriteAllText("JTTT.log", ZawartoscPliku);
        }

    }
}
