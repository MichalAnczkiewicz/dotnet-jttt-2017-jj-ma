﻿using System.Net.Mail;
using System.Net;
using System.Windows.Forms;
using System.Net.Mime;

namespace JTTT
{
    //klasa odpowiadająca za wysyłanie maili
    class MailSender
    {
        public string email { get; set; }

       public virtual void SendEmail() { }
    }

    class MailSenderObrazek : MailSender
    {
        public string tag { get; set; }
        public string obrazekurl { get; set; }

        public  MailSenderObrazek(string _email, string tag, string obrazek)
        {
            email = _email;
            this.tag = tag;
            this.obrazekurl = obrazek;
        }

        public override void SendEmail()
        {
            var message = new MailMessage();
            message.From = new MailAddress("testdotnet12@gmail.com");
            message.To.Add(new MailAddress(this.email));
            message.Subject = "Obrazek na temat: " + this.tag;
            message.Body = "Czesc\n\nObrazek na TEMAT: " + this.tag;

            Attachment zalacznik = new Attachment("obraz.jpg", MediaTypeNames.Application.Octet);
            message.Attachments.Add(zalacznik);

            var smtp = new SmtpClient("smtp.gmail.com");
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("testdotnet12@gmail.com", "dotnettest");
            smtp.EnableSsl = true;
            smtp.Port = 587;
            smtp.Send(message);
            message.Attachments.Dispose();

            MessageBox.Show("Wysłano obrazek na temat: " + tag);
        }
    }
    

    class MailSenderPogoda : MailSender
    {

        public string miasto { get; set; }
        public double temperatura { get; set; }

        public string niebo { get; set; }
        

        public MailSenderPogoda(string email, string miasto, double temperatura/*, double cisnienie*/)
        {
            base.email = email;
            this.miasto = miasto;
            this.temperatura = temperatura;
            //this.cisnienie = cisnienie;
        }

        public override void SendEmail()
        {
            var message = new MailMessage();
            message.From = new MailAddress("testdotnet12@gmail.com");
            message.To.Add(new MailAddress(this.email));
            message.Subject = "Pogoda dla miasta: " + this.miasto;
            message.Body = "Czesc\nPogoda dla miasta " + this.miasto + "\nTemperatura: " + this.temperatura + " stopni celsjusza"; //+ " cisnienie: " + this.cisnienie;

           Attachment zalacznik = new Attachment("pogoda.jpg", MediaTypeNames.Application.Octet);
           message.Attachments.Add(zalacznik);

            var smtp = new SmtpClient("smtp.gmail.com");
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("testdotnet12@gmail.com", "dotnettest");
            smtp.EnableSsl = true;
            smtp.Port = 587;
            smtp.Send(message);
            message.Attachments.Dispose();

            MessageBox.Show("Wysłano aktualną pogodę dla miasta: " + this.miasto);
        }
    }
}
