﻿using System;
using System.Windows.Forms;

namespace JTTT
{

    public partial class Form1 : Form
    {

        ListaZadan zadania = new ListaZadan();
        public Form1()
        {
            InitializeComponent();
            zadaniaBox.DataSource = zadania.Lista_();
        }

        private void wyslijButton_Click(object sender, EventArgs e)
        {


            if(tabControl1.SelectedTab == tabPageStrona)
            {
                if (mailOkno.SelectedTab == tabPageMail)
                {
                    zadania.dodajZadanie(urlTextbox.Text, emailTextbox.Text, tagTextbox.Text, dodajZadanieBox.Text);
                    zadaniaBox.DataSource = zadania.Lista_();
                }
                else
                {
                    zadania.dodajZadanie(urlTextbox.Text, tagTextbox.Text, dodajZadanieBox.Text);
                    zadaniaBox.DataSource = zadania.Lista_();
                }
            }
            else
            {
                if(mailOkno.SelectedTab == tabPageMail)
                {
                    zadania.dodajZadanie(MiastoTextBox.Text, TempNumericUpDown.Value, dodajZadanieBox.Text, emailTextbox.Text);
                    zadaniaBox.DataSource = zadania.Lista_();
                }
                else
                {
                    zadania.dodajZadanie(MiastoTextBox.Text, TempNumericUpDown.Value, dodajZadanieBox.Text);
                    zadaniaBox.DataSource = zadania.Lista_();
                }
            }    
        }

        private void wykonajButton_Click(object sender, EventArgs e)
        {
            zadania.wykonaj();
        }

        private void czyscButton_Click(object sender, EventArgs e)
        {
            zadania.wyczysc();
        }

        //serializacja
        private void serializujButton_Click(object sender, EventArgs e)
        {
            try
            {
                zadania.serializuj();
            }
            catch
            {
                MessageBox.Show("Serializacja zakończona niepowodzeniem");
            }
        }

        //deserializacja
        private void eserializujButton_Click(object sender, EventArgs e)
        {
            try
            {
                zadania.deserializuj();
                zadaniaBox.DataSource = zadania.Lista_();
            }
            catch
            {
                MessageBox.Show("Deserializacja zakończona niepowodzeniem");
            }
        }


        private void CzyscOstatnie_Click(object sender, EventArgs e)
        {
            zadania.CzyscOstatnie();
        }

    }


}