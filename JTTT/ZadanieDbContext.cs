﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    class ZadanieDbContext : DbContext
    {


        public ZadanieDbContext() : base("BazaJTTT")
        {
            Database.SetInitializer(new ZadanieDbInitializer());
        }
        public DbSet<Zadanie> Zadania { get; set; }

    }
}
