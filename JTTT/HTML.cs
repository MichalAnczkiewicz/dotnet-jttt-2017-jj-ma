﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace JTTT
{
    public class HTML
    {
        private readonly string _url;


        public HTML(string url)
        {
            this._url = url;
        }

        public object Messagebox { get; private set; }

        /// <summary>
        /// Prosta metoda, która zwraca zawartość HTML podanej strony www
        /// </summary>
        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                // Ustawiamy prawidłowe kodowanie dla tej strony
                wc.Encoding = Encoding.UTF8;
                // Dekodujemy HTML do czytelnych dla wszystkich znaków 
                string html=null;
                try
                {
                    html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(_url)); // w tym miejscu zglasza wyjątek
                }catch(Exception e)
                {
                    MessageBox.Show("Sprawdz czy podales dobry adres!!!");
                }

                return html;
            }
        }

        /// <summary>
        /// Zwraca pierwszy z listy adresow url znalezionych obrazow, pasujacych do podanej jako parametr frazy
        /// </summary>
        /// <param name="szukanafraza"></param>
        /// <returns></returns>
        public string WezObrazUrl(string szukanafraza)
        {
            szukanafraza = szukanafraza.ToLower();

            List<string> listaadresow = new List<string>();
            var doc = new HtmlAgilityPack.HtmlDocument();
            string pagehhtml;

            pagehhtml = GetPageHtml();
            if (pagehhtml == null)
                return "";
            doc.LoadHtml(pagehhtml);

            var nodes = doc.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {
                string opis = node.GetAttributeValue("alt", "");
                opis=opis.ToLower();
                if (opis.Contains(szukanafraza))
                {
                    string adres = node.GetAttributeValue("src", "");
                    if (adres[0] == '/')
                        adres = _url + adres;
                    listaadresow.Add(adres);
                }
            }

            if (listaadresow.Count > 0)
                return listaadresow[0];
            return "";
        }


        /// <summary>
        /// Pobiera obraz z sieci i zapisuje do pliku o nazwie obraz.[rozszerzenie]
        /// </summary>
        /// <param name="adres">
        /// adres URL skad jest do pobrania obrazek
        /// </param>
        /// <returns>
        /// true - jesli wszystko poszlo dobrze
        /// </returns>
        public bool ZapiszObraz(string adres)
        {
            var webClient = new WebClient();
            byte[] obrazbajtowy=null;
            try
            {
                obrazbajtowy = webClient.DownloadData(adres);
            }catch(Exception e)
            {
                MessageBox.Show("Pobieranie obrazu ze wskazanego adresu nie powiodlo sie :(");
                return false;
            }
            
            //string rozszerzenie = adres.Substring(adres.Length - 3, 3);
            try
            {
                System.IO.File.WriteAllBytes("obraz.jpg", obrazbajtowy);
            } catch(Exception e)
            {
                MessageBox.Show("Operacja zapisu obrazu do pliku nie powiodla sie :(");
                return false;
            }

            return true;
        }


        public Image WezObrazTagu(string tag)
        {
            string obrazekurl = WezObrazUrl(tag);

            var webClient = new WebClient();
            byte[] obrazbajtowy = null;
            try
            {
                obrazbajtowy = webClient.DownloadData(obrazekurl);
            }
            catch (Exception e)
            {
                MessageBox.Show("Pobieranie obrazu ze wskazanego adresu nie powiodlo sie :(");
                return null;
            }

            Image obraz;

            using (var ms = new MemoryStream(obrazbajtowy))
            {
                obraz = Image.FromStream(ms);
            }
            return obraz;
        }
 

    }
}

