﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    class ListaZadan
    {

        BindingList<Zadanie> Lista = new BindingList<Zadanie>();

        public ListaZadan()
        {
            using (var ctx =new ZadanieDbContext())
           {
                foreach(var z in ctx.Zadania)
                {
                    Lista.Add(z);
                }
            }
        }

        public BindingList<Zadanie> Lista_ ()
        {
            return Lista;
        }

        public void dodajZadanie(string url, string email, string tag, string nazwaZadania)
        {
            Zadanie zadanie = new ZadanieStrona(url, email, tag, nazwaZadania);
            Lista.Add(zadanie);
            using (var ctx = new ZadanieDbContext())
            {
               ctx.Zadania.Add(zadanie);
               ctx.SaveChanges();
            }
        }

        public void dodajZadanie(string url, string tag, string nazwaZadania)
        {
            Zadanie zadanie = new ZadanieStrona(url, tag, nazwaZadania);
            Lista.Add(zadanie);
            using (var ctx = new ZadanieDbContext())
            {
                ctx.Zadania.Add(zadanie);
                ctx.SaveChanges();
            }
        }

        public void dodajZadanie(string miasto, decimal temp, string nazwaZadania)
        {
            Zadanie zadanie = new ZadaniePogoda(miasto, temp, nazwaZadania);
            Lista.Add(zadanie);
            using (var ctx = new ZadanieDbContext())
            {
                ctx.Zadania.Add(zadanie);
                ctx.SaveChanges();
            }
        }

        public void dodajZadanie(string miasto, decimal temp, string nazwaZadania, string email)
        {
            Zadanie zadanie = new ZadaniePogoda(miasto, temp, nazwaZadania, email);
            Lista.Add(zadanie);
            using (var ctx = new ZadanieDbContext())
            {
                ctx.Zadania.Add(zadanie);
                ctx.SaveChanges();
            }
        }

        public void wykonaj()
        {
            foreach(Zadanie zadanie in Lista)
            {
                zadanie.wykonajZadanie();
            }
        }

        public void wyczysc()
        {
            using (var ctx = new ZadanieDbContext())
            {
                int ilosc = ctx.Zadania.Count();
                for (int i = ilosc - 1; i >= 0; i--)
                {
                    Zadanie z = ctx.Zadania.First();
                    ctx.Zadania.Remove(z);
                    ctx.SaveChanges();
               }
            }
            Lista.Clear();
        }

        public void CzyscOstatnie()
        {
            if (Lista.Any())
            {
               Zadanie z = Lista.Last();
                using (var ctx = new ZadanieDbContext())
                {
                    if (ctx.Zadania.Count() > 0)
                    {
                        var entry = ctx.Entry(z);
                        if (entry.State == System.Data.Entity.EntityState.Detached)
                            ctx.Zadania.Attach(z);
                        ctx.Zadania.Remove(z);
                        ctx.SaveChanges();
                    }
               }

                Lista.RemoveAt(Lista.Count - 1);
            }
        }

        public void serializuj()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream("MyFile.bin", FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, Lista);
            stream.Close();
        }

        public void deserializuj()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = File.Open("MyFile.bin", FileMode.Open);

            object obj = formatter.Deserialize(fs);
            Lista = (BindingList<Zadanie>)obj;
            fs.Flush();
            fs.Close();
            fs.Dispose();
        }

    }
}
