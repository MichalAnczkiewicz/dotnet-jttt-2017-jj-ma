﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{


    public partial class Form2 : Form
    {
        Pogoda pogoda;
        HTML html;
        string tag;
       
        public Form2(Pogoda pogodap)
        {
            pogoda = pogodap;
            InitializeComponent();
            infoPogodaTextBox.Text = pogoda.ToString();
            pictureBox1.Image = pogoda.WezObrazNieba();
            pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
        }
        public Form2(HTML html1, string tagp)
        {
            tag = tagp;
            html = html1;
            InitializeComponent();
            infoPogodaTextBox.Text = "Obrazek na temat: "+tag ;
            pictureBox1.Image = html.WezObrazTagu(tag);
            pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
