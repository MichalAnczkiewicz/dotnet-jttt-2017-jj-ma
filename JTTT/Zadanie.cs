﻿using System;
using System.Windows.Forms;

namespace JTTT
{
    [Serializable]
    class Zadanie
    {
        public int Id { get; set; }
        public string nazwaZadania { get; set; }
        public string email { get; set; }
        public bool emailCheck { get; set; }
        public override string ToString()
        {
            return "Zadanie: " + nazwaZadania;
        }

        public Zadanie() { }
        public Zadanie(string _nazwaZadania)
        {
            nazwaZadania = _nazwaZadania;
        }

        public virtual void wykonajZadanie() { }

    }

    [Serializable]
    class ZadanieStrona : Zadanie
    {
        public string url { get; set; }
        public string tag { get; set; }
        public ZadanieStrona() { }
        public ZadanieStrona(string _url, string _email, string _tag, string _nazwaZadania)
        {
            url = _url;
            email = _email;
            tag = _tag;
            nazwaZadania = _nazwaZadania;
            emailCheck = true;
        }

        public ZadanieStrona(string _url, string _tag, string _nazwaZadania)
        {
            url = _url;
            tag = _tag;
            nazwaZadania = _nazwaZadania;
            emailCheck = false;
        }

        public override string ToString()
        {   
            if(emailCheck)
                return String.Format(base.ToString()+ " URL: " + url + " Tag: " + tag + " Email: " + email); 
            else
                return String.Format(base.ToString() + " URL: " + url + " Tag: " + tag);

        }

        public override void wykonajZadanie()
        {
            HTML test = new HTML(url);
            string obrazekurl = test.WezObrazUrl(tag);
            Logger logger = new Logger(url, tag, email);

            if (emailCheck)
            {

                if (obrazekurl.Equals(""))
                {
                    MessageBox.Show("Slowa nie znaleziono");
                    logger.Logowanie("Operacja zakończona niepowodzeniem. Nie znaleziono słowa.");
                }
                else
                {
                    if (test.ZapiszObraz(obrazekurl))
                    {
                        MailSender message = new MailSenderObrazek(email, tag, obrazekurl);
                        message.SendEmail();
                        logger.Logowanie(" Operacja wyslania obrazka zakonczona powodzeniem. Pomyslnie wykonano wszystkie działania.");
                    }
                    else
                    {
                        MessageBox.Show("Nie udalo sie zapisac obrazu do pliku");
                        logger.Logowanie("Operacja zakończona niepowodzeniem. Blad zapisu do pliku.");
                    }
                }
            }
            else
            {
                if (obrazekurl.Equals(""))
                {
                    MessageBox.Show("Slowa nie znaleziono");
                    logger.Logowanie("Operacja zakończona niepowodzeniem. Nie znaleziono słowa.");
                }
                else
                {
                    if (test.ZapiszObraz(obrazekurl))
                    {
                        Form2 noweokno = new Form2(test, tag);
                        noweokno.Show();// Dialog();
                        logger.Logowanie(" Operacja wyswietlenia obrazka zakonczona powodzeniem. Pomyslnie wykonano wszystkie działania.");
                    }
                    else
                    {
                        MessageBox.Show("Nie udalo sie zapisac obrazu do pliku");
                        logger.Logowanie("Operacja zakończona niepowodzeniem. Blad odczytu obrazka z pliku.");
                    }

                }
            }
            
        }

        
    }

    [Serializable]
    class ZadaniePogoda : Zadanie
    {
        public string miasto { get; set; }
        public decimal temp { get; set; }
        public ZadaniePogoda() { }
        public ZadaniePogoda(string miastop, decimal tempp, string _nazwaZadania)
        {
            miasto = miastop;
            temp = tempp;
            nazwaZadania = _nazwaZadania;
            emailCheck = false;
        }

        public ZadaniePogoda(string miastop, decimal tempp, string _nazwaZadania, string _email)
        {
            miasto = miastop;
            temp = tempp;
            nazwaZadania = _nazwaZadania;
            email = _email;
            emailCheck = true;
        }

        public override string ToString()
        {
            if(emailCheck)
                return String.Format(base.ToString() + " Miasto: " + miasto + " minimalna temperatura: " + temp + " email " + email);
            else
                return String.Format(base.ToString() + " Miasto: " + miasto + " minimalna temperatura: " + temp );
        }

        public override void wykonajZadanie()
        {
            Logger logger = new Logger();
            Pogoda pogoda = new Pogoda();
            pogoda = pogoda.PobierzPogode(miasto);

            if (emailCheck)
            {
                if (pogoda == null)
                {
                    logger.Logowanie("Operacja zakończona niepowodzeniem. Nie udało się pobrać info o pogodzie");
                    MessageBox.Show("Nie udało się pobrać pogody");
                    return;
                }   
                else
                {
                    pogoda.WezObrazNieba();
                    MailSender message = new MailSenderPogoda(email, miasto, pogoda.main.temp - 273.15);
                    message.SendEmail();
                    logger.Logowanie(" Operacja wyslania pogody zakonczona powodzeniem. Pomyslnie wykonano wszystkie działania.");
                }


            }
            else
            {
                if (pogoda == null)
                {
                    logger.Logowanie("Operacja zakończona niepowodzeniem. Nie udało się pobrać info o pogodzie");
                    return;
                }

                double tempdouble = Double.Parse(temp.ToString());
                if ((pogoda.main.temp - 273.15) > tempdouble)
                {
                    Form2 noweokno = new Form2(pogoda);
                    noweokno.Show();//Dialog();
                }
                else
                {
                    MessageBox.Show("Temperatura w " + pogoda.name + " jest za niska");
                }
                logger.Logowanie("Operacja zakończona powodzeniem.");
            }
        }

    }

}
        


