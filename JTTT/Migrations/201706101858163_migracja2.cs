namespace JTTT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracja2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Zadanies");
            AlterColumn("dbo.Zadanies", "nazwaZadania", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Zadanies", "nazwaZadania");
            DropColumn("dbo.Zadanies", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Zadanies", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Zadanies");
            AlterColumn("dbo.Zadanies", "nazwaZadania", c => c.String());
            AddPrimaryKey("dbo.Zadanies", "Id");
        }
    }
}
