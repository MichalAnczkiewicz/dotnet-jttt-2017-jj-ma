namespace JTTT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracja51 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Zadanies");
            AddColumn("dbo.Zadanies", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Zadanies", "nazwaZadania", c => c.String());
            AddPrimaryKey("dbo.Zadanies", "Id");
            DropColumn("dbo.Zadanies", "nazwaZadaniaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Zadanies", "nazwaZadaniaId", c => c.String(nullable: false, maxLength: 128));
            DropPrimaryKey("dbo.Zadanies");
            DropColumn("dbo.Zadanies", "nazwaZadania");
            DropColumn("dbo.Zadanies", "Id");
            AddPrimaryKey("dbo.Zadanies", "nazwaZadaniaId");
        }
    }
}
