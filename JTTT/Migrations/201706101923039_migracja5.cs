namespace JTTT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracja5 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Zadanies");
            AddColumn("dbo.Zadanies", "nazwaZadaniaId", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Zadanies", "nazwaZadaniaId");
            DropColumn("dbo.Zadanies", "nazwaZadania");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Zadanies", "nazwaZadania", c => c.String(nullable: false, maxLength: 128));
            DropPrimaryKey("dbo.Zadanies");
            DropColumn("dbo.Zadanies", "nazwaZadaniaId");
            AddPrimaryKey("dbo.Zadanies", "nazwaZadania");
        }
    }
}
